import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Layout } from 'antd';
import { withRouter } from 'react-router-dom';
import AppBar from '../components/shared/AppBar';
import LeftDrawer from '../components/shared/LeftDrawer';
import toogleDrawerFunc from '../actions/shared';

class LayoutFull extends PureComponent {
  render() {
    const {
      toogleDrawer, isDrawer, isAuth, children,
    } = this.props;

    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child, this.props));

    return (
      <Layout>
        {isAuth && <LeftDrawer toogleDrawer={toogleDrawer} isDrawer={isDrawer} {...this.props} />}
        <Layout className="application-wrap" style={{ marginLeft: isAuth ? 200 : 0 }}>
          {isAuth && <AppBar isDrawer={isDrawer} toogleDrawer={toogleDrawer} {...this.props} />}
          <Layout.Content className="content">
            <div className="content-wrap" style={{ marginTop: isAuth ? 64 : 0 }}>
              <div className="container content-main">
                {childrenWithProps}
              </div>
            </div>
          </Layout.Content>
        </Layout>
      </Layout>
    );
  }
}


LayoutFull.propTypes = {
  children: PropTypes.any.isRequired,
  toogleDrawer: PropTypes.func.isRequired,
  isDrawer: PropTypes.bool.isRequired,
  isAuth: PropTypes.bool.isRequired,
};


const mapStateToProps = state => ({
  isDrawer: state.shared.isDrawer,
  isAuth: state.user.isAuth,
});

export default compose(
  connect(mapStateToProps, { toogleDrawer: toogleDrawerFunc }),
  withRouter,
)(LayoutFull);
