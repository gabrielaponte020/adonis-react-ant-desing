

/*
|--------------------------------------------------------------------------
| AreaSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Area = use('App/Models/Area');

class AreaSeeder {
  async run() {
    const items = [
      {
        name: 'Marketing',
      },
      {
        name: 'Finanzas',
      },
      {
        name: 'Desarrollo',
      },
      {
        name: 'Gerencia',
      },
    ];

    await Area.createMany(items);
  }
}

module.exports = AreaSeeder;
