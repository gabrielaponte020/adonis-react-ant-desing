

const Schema = use('Schema');

class AreaSchema extends Schema {
  up() {
    this.create('areas', (table) => {
      table.increments();
      table.string('name', 100);
      table.timestamps();
    });
  }

  down() {
    this.drop('areas');
  }
}

module.exports = AreaSchema;
