import React, { PureComponent } from 'react';
import { Form, Input, Modal, Select, Spin } from 'antd';
import PropTypes from 'prop-types';
import { format as rutFormat } from 'rut.js';
import { maskFormat } from '../../util';
import CropViewer from '../common/CropViewer';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 12,
  },
};

class UserForm extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isCropper: false,
      crooperData: null,
      file: null,
      isLoad: false,
    };

    this.crooper = this.crooper.bind(this);
    this.onCrop = this.onCrop.bind(this);
    this.removeFile = this.removeFile.bind(this);
    this.submit = this.submit.bind(this);
  }

  onCrop(data) {
    if (data) {
      this.props.form.setFieldsValue({
        photo: data.getCroppedCanvas().toDataURL(),
      });
      this.setState({ isCropper: false, crooperData: data.getData(), isLoad: true });
    } else {
      this.props.form.setFieldsValue({
        photo: null,
      });
      this.setState({ isCropper: false });
    }
  }

  crooper(data, file) {
    this.props.form.setFieldsValue({
      photo: data,
    });
    this.setState({ isCropper: true, file, isLoad: true });
  }

  removeFile() {
    this.props.form.setFieldsValue({
      photo: null,
    });
    this.setState({ file: null, crooperData: null, isLoad: false });
  }

  submit() {
    const { crooperData, file } = this.state;
    const { validateFields } = this.props.form;
    validateFields((err, fields) => {
      if (!err) {
        this.props.onOk({ ...fields, crooperData, file });
      }
    });
  }

  render() {
    const { isCropper, isLoad } = this.state;

    const {
      getFieldDecorator,
      setFieldsValue,
    } = this.props.form;

    const {
      row,
      show,
      onCancel,
      roles,
      areas,
      loading,
    } = this.props;

    return (
      <Modal
        visible={show}
        title={<h2 className="text-xs-center ma-1" >{row.id ? 'Editar Usuario' : 'Crear Usuario'}</h2>}
        onOk={() => this.submit()}
        onCancel={onCancel}
        maskClosable={false}
        width={570}
        okText="Guardar"
        cancelText="Cancelar"
        okButtonProps={{ loading, disabled: loading }}
        cancelButtonProps={{ loading, disabled: loading }}
        closable={!loading}
        bodyStyle={{
          maxHeight: 600,
          overflow: 'hidden',
          overflowY: 'auto',
        }}
        destroyOnClose
        afterClose={this.removeFile}
      >
        <Spin spinning={loading}>
          <Form id="user-form" onSubmit={e => e.preventDefault()}>
            <FormItem className="text-xs-center">
              {getFieldDecorator('photo', { initialValue: row.photo })(<CropViewer
                size={[300, 300]}
                thumbnailSizes={[150, 150]}
                onCrop={this.onCrop}
                active={this.crooper}
                isCropper={isCropper}
                onRemove={this.removeFile}
                path="/uploads/user/"
                isLoad={isLoad}
              />)}
            </FormItem>
            <FormItem
              className="d-none"
              {...formItemLayout}
            >
              {getFieldDecorator('id', {
                initialValue: row.id,
              })(<Input type="hidden" />)}
            </FormItem>
            <FormItem
              label="Nombre"
              hasFeedback
              {...formItemLayout}
            >
              {getFieldDecorator('name', {
                initialValue: row.name,
                rules: [
                  {
                    required: true,
                    message: 'Campo requerido',
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem label="Apellido paterno" hasFeedback {...formItemLayout}>
              {getFieldDecorator('lastname', {
                initialValue: row.lastname,
              })(<Input />)}
            </FormItem>
            <FormItem label="Apellido materno" hasFeedback {...formItemLayout}>
              {getFieldDecorator('mother_lastname', {
                initialValue: row.mother_lastname,
              })(<Input />)}
            </FormItem>
            <FormItem label="Rut" hasFeedback {...formItemLayout}>
              {getFieldDecorator('rut', {
                initialValue: row.rut && rutFormat(row.rut),
                rules: [
                  {
                    required: true,
                    message: 'Formato de rut invalido',
                  },
                ],
                getValueFromEvent: e => e.target.value && rutFormat(e.target.value),
              })(<Input />)}
            </FormItem>
            <FormItem label="Telefono" hasFeedback {...formItemLayout}>
              {getFieldDecorator('phone', {
                initialValue: row.phone,
                getValueFromEvent: e => maskFormat(e.target.value, '(000) 0000-0000'),
              })(<Input />)}
            </FormItem>
            <FormItem label="Email" hasFeedback {...formItemLayout}>
              {getFieldDecorator('email', {
                initialValue: row.email,
                rules: [
                  {
                    required: true,
                    pattern: /\S+@\S+\.\S+/,
                    message: 'Formato de correo invalido',
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem label="Role" hasFeedback {...formItemLayout}>
              {getFieldDecorator('role_id', {
                initialValue: row.role_id,
                rules: [
                  {
                    required: true,
                    message: 'Campo requerido',
                  },
                ],
              })(<Select
                style={{ width: '100%' }}
                placeholder="Selecciona un role"
                onChange={() => { setFieldsValue({}); }}
              >
                {roles.map((r, i) => <Select.Option key={i} value={r.id}>{r.name}</Select.Option>)}
              </Select>)}
            </FormItem>
            <FormItem label="Area" hasFeedback {...formItemLayout}>
              {getFieldDecorator('area_id', {
                initialValue: row.area_id,
                rules: [
                  {
                    required: true,
                    message: 'Campo requerido',
                  },
                ],
              })(<Select
                style={{ width: '100%' }}
                placeholder="Selecciona un role"
                onChange={() => { setFieldsValue({}); }}
              >
                {areas.map((r, i) => <Select.Option key={i} value={r.id}>{r.name}</Select.Option>)}
              </Select>)}
            </FormItem>
          </Form>
        </Spin>
      </Modal>
    );
  }
}


UserForm.propTypes = {
  form: PropTypes.object.isRequired,
  row: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onOk: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  roles: PropTypes.array.isRequired,
  areas: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default Form.create()(UserForm);

