const Role = use('App/Models/Role');

class RoleController {
  /**
   * List
   * @returns {Promise<*>}
   */
  async index() {
    const roles = await Role.query();

    return roles;
  }
  /**
   * Find role
   * @param request
   * @returns {Promise<*>}z
   */
  async show({ request }) {
    const { id } = request.all();
    const roles = await Role.find(id);
    return roles;
  }

  /**
   * Create role
   * @param request
   * @returns {Promise<*>}
   */
  async store({ request }) {
    const role = request.only(['type', 'company_id', 'name', 'lastname', 'photo', 'email', 'password']);
    const result = await Role.create(role);
    return result;
  }

  /**
   * Update role
   * @param request
   * @returns {Promise<void>}
   */
  async update({ request, auth }) {
    const {
      id,
      name,
      lastname,
      email,
      type,
      password,
      company_id,
    } = request.all();
    const role = await Role.find(id);
    role.merge({
      id,
      name,
      lastname,
      email,
      type,
      password,
      company_id,
    });
    await role.save();

    const authrole = await auth.generate(role, true);

    return authrole;
  }

  /**
   * Delete role
   * @param request
   * @returns {Promise<void>}
   */
  async destroy({ params }) {
    const role = await Role.find(params.id);
    await role.delete();
  }
}

module.exports = RoleController;
