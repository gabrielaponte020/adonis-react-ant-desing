

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Role = use('App/Models/Role');

class RoleSeeder {
  async run() {
    const items = [
      {
        name: 'Adminsitrador',
        menus: '*',
      },
      {
        name: 'Supervisor',
        menus: '*',
      },
      {
        name: 'Editor',
        menus: '*',
      },
      {
        name: 'Espectador',
        menus: '*',
      },
    ];

    await Role.createMany(items);
  }
}

module.exports = RoleSeeder;
