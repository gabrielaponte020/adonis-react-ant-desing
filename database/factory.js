

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory');

Factory.blueprint('App/Models/User', async faker => ({
  name: faker.first(),
  area_id: faker.integer({ min: 2, max: 4 }),
  lastname: faker.last(),
  mother_lastname: faker.last(),
  email: faker.email(),
  password: '123456',
  phone: faker.phone(),
  rut: faker.cpf(),
  role_id: faker.integer({ min: 2, max: 4 }),
}));
