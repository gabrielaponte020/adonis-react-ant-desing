const User = use('App/Models/User');
const Token = use('App/Models/Token');
const Drive = use('Drive');
const Helpers = use('Helpers');
const isEmpty = require('lodash/isEmpty');
const gm = require('gm');


const formatRut = require('rut.js');

// Path to save file of user
const storePath = '/uploads/user';

class UserController {
  /**
   * List Users
   * @returns {Promise<*>}
   */
  async index({ request, auth }) {
    const { user } = auth.current;

    const {
      page = 1,
      rowsPerPage = 10,
      sortField = 'name',
      sortOrder = 'asc',
      search = '',
      searchBy = '\'\'',
    } = request.all();
    const users = await User.query()
      .with('role')
      .with('area')
      .where('id', '<>', user.id)
      .whereRaw(`CASE ${searchBy || '\'\''} WHEN '' THEN 1 WHEN ${searchBy || '\'\''} THEN CASE WHEN ${searchBy || '\'\''} LIKE '%${search}%' THEN 1 ELSE 0 END END = 1`)
      .orderBy(sortField, sortOrder)
      .paginate(page, rowsPerPage === '-1' ? 99999999999 : rowsPerPage);
    return users;
  }
  /**
   * Find User
   * @param request
   * @returns {Promise<*>}z
   */
  async show({ request }) {
    const { id } = request.all();
    const users = await User.find(id);
    return users;
  }

  /**
   * Create User
   * @param request
   * @returns {Promise<*>}
   */
  async store({ request }) {
    const user = request.only([
      'name',
      'lastname',
      'mother_lastname',
      'rut',
      'phone',
      'email',
      'photo',
      'password',
      'area_id',
      'role_id',
    ]);

    let crooperData = request.input('crooperData');

    user.password = formatRut.clean(user.rut);

    // Get file
    const file = request.file('file', {
      types: ['image'],
      size: '2mb',
    });

    // Move file to uploads folder
    if (file) {
      user.photo = `${new Date().getTime()}.${file.subtype}`;
      await file.move(Helpers.publicPath(storePath), {
        name: user.photo,
      });
      if (!file.move() || !isEmpty(file.error())) {
        throw file.error();
      }
      crooperData = JSON.parse(crooperData);
      gm(`${Helpers.publicPath(storePath)}/${user.photo}`)
        .crop(crooperData.width, crooperData.height, crooperData.x, crooperData.y)
        .write(`${Helpers.publicPath(storePath)}/${user.photo}`, (err) => {
          if (err) {
            console.log('gm-error', err);
          }
        });
    }
    const result = await User.create(user);
    return result;
  }

  /**
   * Update User
   * @param request
   * @returns {Promise<void>}
   */
  async update({ request }) {
    const fields = request.only([
      'id',
      'name',
      'lastname',
      'mother_lastname',
      'rut',
      'phone',
      'email',
      'photo',
      'area_id',
      'role_id',
      'active',
    ]);
    let crooperData = request.input('crooperData');

    const user = await User.find(fields.id);

    let { photo } = user;

    // Get file
    const file = request.file('file', {
      types: ['image'],
      size: '2mb',
    });

    // Move file to uploads folder
    if (file) {
      photo = `${new Date().getTime()}.${file.subtype}`;
      await file.move(Helpers.publicPath(storePath), {
        name: photo,
      });

      crooperData = JSON.parse(crooperData);

      if (!file.move() || !isEmpty(file.error())) {
        throw file.error();
      } else if (user.photo) {
        await Drive.delete(`${Helpers.publicPath(storePath)}/${user.photo}`);
        gm(`${Helpers.publicPath(storePath)}/${photo}`)
          .crop(crooperData.width, crooperData.height, crooperData.x, crooperData.y)
          .write(`${Helpers.publicPath(storePath)}/${photo}`, (err) => {
            if (err) {
              console.log('gm-error', err);
            }
          });
      }
    }

    // Set photo result
    fields.photo = photo;

    user.merge(fields);
    await user.save();
    return user;
  }

  async resetPassword({ request, auth }) {
    const authUser = await auth.generate(user, true);

    return authUser;
  }

  /**
   * Delete User
   * @param request
   * @returns {Promise<void>}
   */
  async destroy({ params }) {
    await Token.query().where('user_id', params.id).delete();
    const user = await User.find(params.id);
    await user.delete();
  }

  /**
   * Login
   * @param request
   * @param auth
   * @returns {Promise<*>}
   */
  async login({ request, auth }) {
    const { email, password } = request.all();
    const token = await auth
      .withRefreshToken()
      .attempt(email, password, true);

    return token;
  }
}

module.exports = UserController;
