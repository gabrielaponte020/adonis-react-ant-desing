import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon, Card, Skeleton } from 'antd';
import CountUp from 'react-countup';

class NumberCard extends Component {
  render() {
    const {
      icon,
      color,
      title,
      number,
      loading,
    } = this.props;
    return (
      <Card className="number-card" bodyStyle={{ padding: 0 }} hoverable>
        <Skeleton paragraph={0} className="pt-2" loading={loading} active avatar>
          <Icon className="icon-warp" style={{ color }} type={icon} />
          <div className="content-number">
            <p className="title-number">{title}</p>
            <p className="number">
              <CountUp
                start={0}
                end={number}
                duration={2.75}
                useEasing
                useGrouping
                separator=","
              />
            </p>
          </div>
        </Skeleton>
      </Card>
    );
  }
}

NumberCard.propTypes = {
  icon: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default NumberCard;
