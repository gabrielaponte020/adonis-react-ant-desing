import { SET_CURRENT_USER } from './types';
import { setAuthorizationToken } from '../util';

const localUser = '@user';

export function setCurrentUser(user) {
  return async (dispatch) => {
    localStorage.setItem(localUser, JSON.stringify(user));
    await dispatch({ type: SET_CURRENT_USER, user });
    setAuthorizationToken(user);
  };
}

export function logout() {
  return async (dispatch) => {
    localStorage.removeItem(localUser);
    await dispatch({ type: SET_CURRENT_USER });
  };
}

/**
 * Method for get localstore User
 * @returns {null}
 */
export function getLocalUser() {
  const user = localStorage.getItem(localUser);
  return (user ? JSON.parse(user) : null);
}

export function set() {
  const user = localStorage.getItem(localUser);
  return (user ? JSON.parse(user) : null);
}
