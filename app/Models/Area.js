

const Model = use('Model');

class Area extends Model {
  users() {
    return this.hasMany('App/Models/Area', 'id', 'area_id');
  }
}

module.exports = Area;
