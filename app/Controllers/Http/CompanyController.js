

const isEmpty = require('lodash/isEmpty');

const Company = use('App/Models/Company');
const Helpers = use('Helpers');
const Drive = use('Drive');

const storePath = '/uploads/company';

class CompanyController {
  async index({ request }) {
    const {
      page = 1, rowsPerPage = 10, sortBy = 'name', descending = 'asc', search = '', type = 0,
    } = request.all();
    const companies = await Company.query()
      .with('users')
      .whereRaw('name like  ?', [`%${search}%`])
      .where('type', type > 0 ? '=' : '>', type)
      .orderBy(sortBy, descending)
      .paginate(page, rowsPerPage === '-1' ? 99999999999 : rowsPerPage);
    return companies;
  }

  /**
   * Create Company
   * @param request
   * @returns {Promise<*>}
   */
  async store({ request }) {
    const company = request.only(['name', 'logo', 'description', 'type']);

    // Get CompanyImage
    const file = request.file('file', {
      types: ['image'],
      size: '2mb',
    });

    // Move image to uploads folder
    if (file) {
      company.logo = `${new Date().getTime()}.${file.subtype}`;
      await file.move(Helpers.publicPath(storePath), {
        name: company.logo,
      });
      if (!file.move() || !isEmpty(file.error())) {
        throw file.error();
      }
    }

    const result = await Company.create(company);
    return result;
  }

  /**
   * Find Company
   * @param request
   * @returns {Promise<*>}
   */
  async show({ request }) {
    const { id } = request.all();
    const companies = await Company.find(id);
    return companies;
  }

  /**
   * Update Company
   * @param request
   * @returns {Promise<void>}
   */
  async update({ request }) {
    const {
      id,
      name,
      logo,
      description,
      type,
    } = request.all();
    const company = await Company.find(id);

    let logoName = logo;

    // Get CompanyImage
    const file = request.file('file', {
      types: ['image'],
      size: '2mb',
    });

    // Move image to uploads folder
    if (file) {
      logoName = `${new Date().getTime()}.${file.subtype}`;
      await file.move(Helpers.publicPath(storePath), {
        name: logoName,
      });


      if (!file.move() || !isEmpty(file.error())) {
        throw file.error();
      } else if (company.logo) {
        await Drive.delete(`${storePath}/${company.logo}`);
      }
    }

    company.merge({
      id,
      name,
      logo: logoName,
      description,
      type,
    });
    await company.save();

    return company;
  }

  /**
   * Delete Company
   * @param request
   * @returns {Promise<void>}
   */
  async destroy({ params }) {
    const company = await Company.find(params.id);
    if (company.logo) await Drive.delete(`${storePath}/${company.logo}`);
    await company.delete();
  }
}

module.exports = CompanyController;
