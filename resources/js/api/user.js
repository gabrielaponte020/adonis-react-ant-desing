import axios from 'axios';
import { apiUrl } from './';

export default {
  async login(params) {
    return axios.post(`${apiUrl}/user/login`, params);
  },
  async fetch(params) {
    return axios.get(`${apiUrl}/user`, { params });
  },
  async create(params) {
    return axios.post(`${apiUrl}/user`, params);
  },
  async update(id, params) {
    return axios.put(`${apiUrl}/user/${id}`, params);
  },
  async delete(id) {
    return axios.delete(`${apiUrl}/user/${id}`);
  },
  async inCompany(params) {
    return axios.get(`${apiUrl}/user/in-company`, { params });
  },
};
