import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import Loadable from 'react-loadable';

import Loading from '../../components/common/Loading';

const UserList = Loadable({
  loader: () => import(/* webpackChunkName: "user-list" */'../../components/user/UserList'),
  loading: () => <Loading size="large" />,
});

class Admin extends Component {
  render() {
    return (
      <UserList />
    );
  }
}

UserList.propTypes = {
  // history: PropTypes.object.isRequired,
};

export default Admin;
