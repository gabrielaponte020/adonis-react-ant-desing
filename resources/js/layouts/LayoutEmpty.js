import React from 'react';
import PropTypes from 'prop-types';

class LayoutEmpty extends React.PureComponent {
  render() {
    return (
      this.props.children
    );
  }
}

LayoutEmpty.propTypes = {
  children: PropTypes.element.isRequired,
};

export default LayoutEmpty;
