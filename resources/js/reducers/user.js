import isEmpty from 'lodash/isEmpty';
import jwt from 'jsonwebtoken';
import { SET_CURRENT_USER } from '../actions/types';

const initialState = {
  isAuth: false,
  user: {},
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        isAuth: !isEmpty(action.user),
        user: !isEmpty(action.user) ?
          { ...action.user, ...(jwt.decode(action.user.token)) } :
          {},
      };
    default: return state;
  }
};
