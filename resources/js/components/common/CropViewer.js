/* eslint react/require-default-props: 0 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Tooltip } from 'antd';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';
import '../../../scss/cropper.scss';

class CropViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFile: false,
    };
    this.cropperComponent = React.createRef();
    this.onChangeFile = this.onChangeFile.bind(this);
  }

  onChangeFile(e) {
    if (e.target.files) {
      const reader = new FileReader();
      const file = e.target.files[0];

      this.setState({ isFile: true });

      reader.onloadend = () => {
        this.props.active(reader.result, file);
      };

      reader.readAsDataURL(file);
    } else {
      this.setState({ isFile: false });
    }
  }

  render() {
    const {
      size,
      thumbnailSizes,
      onCrop,
      value,
      isCropper,
      onRemove,
      path,
    } = this.props;

    return (
      <div className="text-xs-center">
        {isCropper ?
          <Modal
            visible={isCropper}
            onCancel={() => onCrop()}
            onOk={() => onCrop(this.cropperComponent.current)}
          >
            <div className="d-flex justify-center align-center pa-5">
              <Cropper
                ref={this.cropperComponent}
                src={value}
                style={{ width: size[0], height: size[1] }}
                // Cropper.js options
                aspectRatio={1 / 1}
                guides={false}
                multiple={false}
                directory={false}
                rotatable={false}
                cropBoxResizable={false}
                scalable={false}
                highlight={false}
                checkOrientation={false}
                dragMode="move"
                viewMode={1}
                drop
                dropDirectory
                thread={3}
              />
            </div>
          </Modal> :
          <div
            className="priview-wrapper "
            style={{ width: thumbnailSizes[0] + 18, height: thumbnailSizes[1] + 18 }}
          >
            <div
              className="preview"
              style={{ width: thumbnailSizes[0], height: thumbnailSizes[1] }}
            >

              {value ?
                <div>
                  <Tooltip placement="top" title="Remover" >
                    <div
                      tabIndex={0}
                      role="button"
                      className="preview-mask"
                      style={{ width: thumbnailSizes[0], height: thumbnailSizes[1] }}
                      onClick={onRemove}
                    >
                      <Icon type="delete" />
                    </div>
                  </Tooltip>
                  <img
                    className="circle"
                    src={this.state.isFile ? value : `${path}${value}`}
                    alt={value}
                    style={{ width: thumbnailSizes[0], height: thumbnailSizes[1] }}
                  />
                </div> :
                <Tooltip placement="top" title="Cargar archivo" >
                  <input type="file" id="file" className="inputfile" onChange={this.onChangeFile} />
                  <label htmlFor="file" style={{ width: thumbnailSizes[0], height: thumbnailSizes[1] }} >
                    <Icon type="upload" style={{ fontSize: thumbnailSizes[0] - (thumbnailSizes[0] / 2) }} />
                  </label>
                </Tooltip>
                }
            </div>
          </div>

        }
      </div>
    );
  }
}

CropViewer.defaultProps = {
  size: [150, 150],
  thumbnailSizes: [64, 64],
  path: null,
};

CropViewer.propTypes = {
  size: PropTypes.array,
  thumbnailSizes: PropTypes.array,
  onCrop: PropTypes.func.isRequired,
  active: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  value: PropTypes.any,
  isCropper: PropTypes.bool.isRequired,
  path: PropTypes.string,
};


export default CropViewer;
