

const Schema = use('Schema');

class CompanySchema extends Schema {
  up() {
    this.create('companies', (table) => {
      table.increments();
      table.string('name', 100).notNullable();
      table.string('logo', 80);
      table.string('description', 250);
      table.timestamps();
    });
  }

  down() {
    this.drop('companies');
  }
}

module.exports = CompanySchema;
