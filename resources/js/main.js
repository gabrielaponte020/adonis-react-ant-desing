import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { AppContainer } from 'react-hot-loader';
import 'antd/dist/antd.css';
import registerServiceWorker from './registerServiceWorker';
import reducers from './reducers';
import routes from './routes/';
import App from './App';
import '../css/vuetify.css';
import '../scss/site.scss';

// Store
const store = createStore(
  reducers,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
);

// Render root
const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <AppContainer>
        <App routes={routes} />
      </AppContainer>
    </Provider>,
    document.getElementById('root'),
  );
};

registerServiceWorker();

// First render
render(routes);

// On hot Reload re-render
if (module.hot) {
  module.hot.accept('./routes/', () => {
    const newRoutes = require('./routes/').default;
    render(newRoutes);
  });
}
