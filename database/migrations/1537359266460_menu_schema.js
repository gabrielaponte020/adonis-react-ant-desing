

const Schema = use('Schema');

class MenuSchema extends Schema {
  up() {
    this.create('menus', (table) => {
      table.increments();
      table.string('name', 150).notNullable();
      table.boolean('active').defaultTo(true);
      table.timestamps();
    });
  }

  down() {
    this.drop('menus');
  }
}

module.exports = MenuSchema;
