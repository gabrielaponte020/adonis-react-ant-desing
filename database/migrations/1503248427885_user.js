

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema');

class UserSchema extends Schema {
  async up() {
    this.create('users', (table) => {
      table.increments();
      table.integer('area_id').unsigned();
      table.integer('role_id').unsigned();
      table.boolean('active').defaultTo(true);
      table.string('rut', 80).notNullable().unique();
      table.string('name', 80).notNullable();
      table.string('lastname', 80);
      table.string('mother_lastname', 80);
      table.string('photo');
      table.string('phone');
      table.string('email', 254).notNullable().unique();
      table.string('password', 60).notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop('users');
  }
}

module.exports = UserSchema;

