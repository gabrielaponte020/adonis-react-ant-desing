

const Schema = use('Schema');

class RoleSchema extends Schema {
  up() {
    this.dropIfExists('roles');
    this.create('roles', (table) => {
      table.increments();
      table.string('name', 80).notNullable();
      table.text('menus');
      table.timestamps();
    });
  }

  down() {
    this.drop('roles');
  }
}

module.exports = RoleSchema;
