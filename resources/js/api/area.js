import axios from 'axios';
import { apiUrl } from './';

export default {
  async fetch(params) {
    return axios.get(`${apiUrl}/area`, { params });
  },
  async create(params) {
    return axios.post(`${apiUrl}/area`, params);
  },
  async update(params) {
    return axios.put(`${apiUrl}/area/${params.id}`, params);
  },
  async delete(id) {
    return axios.delete(`${apiUrl}/area/${id}`);
  },
};
