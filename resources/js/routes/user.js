import React from 'react';
import { Switch } from 'react-router-dom';
import { requireAuth as AuthRute, notRequireAuth as NoAtuhRoute } from '../util/index';
import Login from '../pages/user/Login';
import Admin from '../pages/user/Admin';

/**
 * Routes for auth module
 */
export default (
  <Switch key="user-switch">
    <NoAtuhRoute
      key="login"
      path="/auth/login"
      component={Login}
    />
    <AuthRute
      key="user-admin"
      path="/user/admin"
      component={Admin}
    />
  </Switch>
);
