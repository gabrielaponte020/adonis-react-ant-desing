import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';


class Loading extends Component {
  render() {
    const { size = 'small' } = this.props;
    return (
      <div className="d-flex justify-center align-center fill-height">
        <Spin size={size} />
      </div>
    );
  }
}

Loading.propTypes = {
  size: PropTypes.string.isRequired,
};


export default Loading;
