import axios from 'axios';
import { apiUrl } from './';

export default {
  async fetch(params) {
    return axios.get(`${apiUrl}/role`, { params });
  },
  async create(params) {
    return axios.post(`${apiUrl}/role`, params);
  },
  async update(params) {
    return axios.put(`${apiUrl}/role/${params.id}`, params);
  },
  async delete(id) {
    return axios.delete(`${apiUrl}/role/${id}`);
  },
};
