import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Layout, Row, Col } from 'antd';
import NumberCard from '../dashboard/NumberCard';

/*
numbers: [
  {
    icon: 'pay-circle-o',
    color: '#64ea91',
    title: 'Empleados totales',
    number: 2781,
  }, {
    icon: 'team',
    color: '#8fc9fb',
    title: 'Empleados Activos',
    number: 3241,
  }, {
    icon: 'message',
    color: '#d897eb',
    title: 'Saldo',
    number: 253,
  }, {
    icon: 'shopping-cart',
    color: '#f69899',
    title: 'Referrals',
    number: 4324,
  },
],
*/

class Indicators extends Component {
  render() {
    const { data, loading } = this.props;
    return (
      data.map((item, key) =>
        (<Layout>
          <Row gutter={16}>
            <Col key={key} lg={6} md={12}>
              <NumberCard {...item} loading={loading} />
            </Col>
          </Row>
        </Layout>)));
  }
}

Indicators.propTypes = {
  data: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default Indicators;
