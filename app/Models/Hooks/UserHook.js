
const Hash = use('Hash');
const Drive = use('Drive');
const Helpers = use('Helpers');
const isEmpty = require('lodash/isEmpty');
const { validate, clean, format } = require('rut.js');

const storePath = '/uploads/user';


module.exports = {};
const UserHook = module.exports;

/**
 * Hash using password as a hook.
 *
 * @method
 *
 * @param  {Object} userInstance
 *
 * @return {void}
 */
UserHook.hashPassword = async (userInstance) => {
  if (userInstance.password) {
    userInstance.password = await Hash.make(await Hash.clear(userInstance.password));
  }
};

UserHook.commonFields = async (userInstance) => {
  if (!isEmpty(userInstance.active)) {
    userInstance.active = (/true/i).test(userInstance.active) ? 1 : 0;
  }

  if (!isEmpty(userInstance.rut)) {
    userInstance.rut = clean(userInstance.rut);
  }
};


UserHook.deleteFiles = async (userInstance) => {
  if (userInstance.photo) {
    await Drive.delete(`${Helpers.publicPath(storePath)}/${userInstance.photo}`);
  }
};
