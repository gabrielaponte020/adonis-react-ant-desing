import { combineReducers } from 'redux';
import { getJsModules } from '../util/index';

export default combineReducers(getJsModules(require.context('.', false, /\.js$/)));
