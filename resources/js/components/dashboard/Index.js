import React, { Component } from 'react';
import logo from '../logo.svg';
import Layout from '../layouts/Layout';

class Index extends Component {
  render() {
    return (
      <Layout>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
          </header>
        </div>
      </Layout>
    );
  }
}

export default Index;
