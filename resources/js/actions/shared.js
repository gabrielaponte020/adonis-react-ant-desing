import { TOOGLE_DRAWER } from './types';

/**
 * Method for toogle  drawer in state
 * @param isDrawer
 * @returns {function(*)}
 */
export default function toogleDrawer(isDrawer) {
  return (dispatch) => {
    dispatch({ type: TOOGLE_DRAWER, isDrawer });
  };
}
