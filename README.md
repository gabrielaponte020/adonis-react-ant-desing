## Requerimientos

Instalar GraphicsMagick para el cooping de imagenes

####Windows
Con chocolatey
```bash
choco install graphicsmagick
```
Descargar .exe

ftp://ftp.graphicsmagick.org/pub/GraphicsMagick/windows/

####Linux

```bash
sudo apt-get install graphicsmagick
```

## Enviroments

Copiar los .examples

.env para el front
 
```bash
.env.front
```

.env para el back
 
```bash
.env
```

## WebServer (Desarrollo)

Levantar el Backend

```bash
npm run dev
```

Levantar el Frontend

```bash
npm run front
```

## Build (Produccion)

Compilar Front

```bash
npm run build
```

Levantar Serve

```bash
npm start
```

### Migraciones

Ejecutar Migracion

```bash
npm run migrate
```

Ejecutar Seed

```bash
npm run seed
```

Resetear (Esto Borra las migraciones, las ejecuta de nuevo y corre los seed)
```bash
npm run reset
```
