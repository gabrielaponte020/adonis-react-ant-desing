import React, { Component } from 'react';
import {
  Table,
  Layout,
  Button,
  Card,
  Row,
  Col,
  Switch,
  Popconfirm,
  Tooltip,
  Input,
  Icon,
  message,
  Avatar,
  Select,
} from 'antd';
import { format as formatRut } from 'rut.js';

import UserForm from './UserForm';
import api from '../../api/user';
import apiRole from '../../api/role';
import apiAreas from '../../api/area';
import { dateFormat } from '../../util';

let clearAllFilters = () => null;

class UserList extends Component {
  constructor(props) {
    super(props);
    this.userForm = React.createRef();

    this.state = {
      loading: true,
      sortField: null,
      sortOrder: null,
      users: {
        data: [],
        lastPage: 1,
        page: 1,
        perPage: 10,
        total: 1,
      },
      roles: [],
      columns: [],
      row: {},
      show: false,
      search: null,
      searchBy: null,
      areas: [],
    };

    this.editUser = this.editUser.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    this.saveUser = this.saveUser.bind(this);
    this.fetchUsers = this.fetchUsers.bind(this);
    this.fetchRoles = this.fetchRoles.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.fetchAreas = this.fetchAreas.bind(this);
    this.addUser = this.addUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.buildColumns = this.buildColumns.bind(this);
    this.clearFilters = this.clearFilters.bind(this);
  }

  async componentDidMount() {
    await this.fetchRoles();
    await this.fetchAreas();
    this.buildColumns();
    await this.fetchUsers();
  }

  buildColumns() {
    const { areas, roles } = this.state;
    const searchFilter = (key, type, items) => ({
      filterDropdown: ({
        setSelectedKeys, selectedKeys, confirm, clearFilters,
      }) => (<div className="custom-filter-dropdown">
        {type === 'select' ?
          <Select
            ref={ele => this[`searchInput-${key}`] = ele}
            style={{ width: 130, marginRight: 8 }}
            placeholder="Selecciona"
            onChange={(val) => {
                clearAllFilters = clearFilters;
                const value = val ? [val] : [];
                setSelectedKeys(value);
                this.handleSearch(value, key, confirm);
              }}
          >
            {items.map((r, i) => <Select.Option key={i} value={r.id}>{r.name}</Select.Option>)}
          </Select>
            :
          <Input
            ref={ele => this[`searchInput-${key}`] = ele}
            value={selectedKeys[0]}
            onChange={(e) => {
                clearAllFilters = clearFilters;
                setSelectedKeys(e.target.value ? [e.target.value] : []);
              }}
            onPressEnter={() => {
                this.handleSearch(selectedKeys, key, confirm);
              }}
          />
          }

        <Button
          type="primary"
          onClick={() => {
              this.handleSearch(selectedKeys, key, confirm);
            }}
          icon="search"
        />
        <Button
          onClick={() => {
              this.handleReset(clearFilters);
            }}
          icon="close"
        />
      </div>
      ),
      filterIcon: filtered => <Icon type={type === 'select' ? 'filter' : 'search'} style={{ color: filtered ? '#108ee9' : '#aaa' }} />,
      onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
          setTimeout(() => {
            this[`searchInput-${key}`].focus();
          });
        }
      },
    });
    this.setState({
      columns: [
        {
          title: 'Foto',
          dataIndex: 'photo',
          key: 'photo',
          render: (item) => {
            const obj = {};
            if (item) obj.src = `/uploads/user/${item}`;
            else obj.icon = 'user';
            return <Avatar size="large" {...obj} />;
          },
        },
        {
          title: 'Area',
          dataIndex: 'area',
          key: 'area',
          render: item => (item ? item.name : ''),
          ...searchFilter('area_id', 'select', areas),
        },
        {
          title: 'Rut',
          dataIndex: 'rut',
          key: 'rut',
          sorter: true,
          ...searchFilter('rut'),
          render: item => formatRut(item),
        },
        {
          title: 'Nombre',
          dataIndex: 'name',
          key: 'name',
          sorter: true,
          ...searchFilter('name'),
        },
        {
          title: 'Apellido Paterno',
          dataIndex: 'lastname',
          key: 'lastName',
          ...searchFilter('lastName'),
        },
        {
          title: 'Apellido Materno',
          dataIndex: 'mother_lastname',
          key: 'mother_lastname',
          ...searchFilter('mother_lastname'),
        },
        {
          title: 'Email',
          dataIndex: 'email',
          key: 'email',
          ...searchFilter('email'),
        },
        {
          title: 'Telefono',
          dataIndex: 'phone',
          key: 'phone',
          ...searchFilter('phone'),
        },
        {
          title: 'Fecha ultima actualización',
          dataIndex: 'updated_at',
          key: 'updated_at',
          render: item => dateFormat(item),
        },
        {
          title: 'Role',
          dataIndex: 'role',
          key: 'role',
          render: item => (item ? item.name : ''),
          ...searchFilter('role_id', 'select', roles),
        },
        {
          title: <div className="text-xs-center"><span>Acción</span></div>,
          key: 'action',
          fixed: 'right',
          width: 130,
          render: (item, row) =>
            (
              <Row gutter={8}>
                <Col xs={8}>
                  <Tooltip placement="topLeft" title={row.active === 1 ? 'Desactivar' : 'Activar'}>
                    <Switch
                      onChange={() => {
                        row.active = !row.active;
                        this.saveUser(row);
                      }}
                      size="small"
                      checked={row.active === 1}
                      className={row.active === 1 ? 'green' : 'red'}
                    />
                  </Tooltip>
                </Col>
                <Col xs={8}>
                  <Tooltip placement="topLeft" title="Editar">
                    <Button onClick={() => this.editUser(row)} shape="circle" icon="edit" />
                  </Tooltip>
                </Col>
                <Col xs={8}>
                  <Popconfirm
                    title="Estas seguro?"
                    onConfirm={() => this.deleteUser(row.id)}
                    cancelText="Cancelar"
                    okText="Si"
                  >
                    <Tooltip placement="topLeft" title="Eliminar">
                      <Button shape="circle" icon="close" type="danger" />
                    </Tooltip>
                  </Popconfirm>
                </Col>
              </Row>
            ),
        },
      ],
    });
  }

  clearFilters() {
    clearAllFilters();
    this.setState({ search: '', searchBy: '' }, () => {
      this.fetchUsers();
    });
  }

  /**
   * Fetch users to list
   * @param page
   * @param rowsPerPage
   */
  async fetchUsers(page, rowsPerPage) {
    const {
      search,
      users,
      searchBy,
      sortField,
      sortOrder,
    } = this.state;
    this.setState({ loading: true }, async () => {
      try {
        const { data } = await api.fetch({
          page: page || users.page,
          rowsPerPage: rowsPerPage || users.perPage,
          search,
          searchBy,
          sortField,
          sortOrder,
        });
        this.setState({ users: data, loading: false });
      } catch (e) {
        this.setState({ loading: false }, () => {
          message.error('Ocurrio un error al optener los usuarios, intente de nuevo.');
        });
      }
    });
  }


  /**
   * Open Modal to add user
   */
  addUser() {
    this.userForm.current.resetFields();
    this.setState({ show: true });
    this.setState({ row: {}, show: true });
  }

  /**
   * Open Modal to edit user
   * @param row (User)
   */
  editUser(row) {
    this.userForm.current.resetFields();
    this.setState({ row, show: true });
  }

  cancelEdit() {
    this.setState({ show: false, row: {} });
  }

  /**
   * Save User
   * @param user
   */
  saveUser(fields) {
    this.setState({ loading: true }, async () => {
      try {
        if (fields.file) fields.photo = null;
        const keys = Object.keys(fields);
        const formData = new FormData();
        if (fields.file !== undefined) {
          formData.append('file', fields.file);
        }
        keys.forEach((k) => {
          formData.append(k, (typeof fields[k] === 'object' ? JSON.stringify(fields[k]) : fields[k]));
        });
        if (fields.id) await api.update(fields.id, formData);
        else await api.create(formData);
        await this.fetchUsers();

        this.setState({ loading: false, show: false });
      } catch (e) {
        this.setState({ loading: false });
      }
    });
  }

  async fetchRoles() {
    try {
      const { data } = await apiRole.fetch();
      this.setState({ roles: data });
    } catch (e) {
      message.error('Ocurrio un error al optener los roles.');
    }
  }

  async fetchAreas() {
    try {
      const { data } = await apiAreas.fetch();
      this.setState({ areas: data });
    } catch (e) {
      message.error('Ocurrio un error al optener las areas.');
    }
  }

  handleSearch(selectedKeys, searchBy, confirm) {
    confirm();
    this.setState({ search: selectedKeys[0], searchBy }, () => {
      this.fetchUsers();
    });
  }

  handleReset() {
    console.log(clearAllFilters);
    clearAllFilters();
    this.setState({ search: '', searchBy: '' }, () => {
      this.fetchUsers();
    });
  }

  /**
   * Delete User
   * @param user
   */
  deleteUser(id) {
    this.setState({ loading: true }, async () => {
      try {
        await api.delete(id);
        await this.fetchUsers();
        this.setState({ loading: false });
      } catch (e) {
        this.setState({ loading: false });
      }
    });
  }
  render() {
    const {
      columns,
      users,
      loading,
      row,
      show,
      roles,
      areas,
    } = this.state;

    return (
      <Layout>
        <Row>
          <Card
            bodyStyle={{ padding: 0 }}
            title={<h2>Usuarios</h2>}
            extra={
              <Row gutter={8}>
                <Col xs={8}>
                  <Tooltip placement="top" title="Agregar usuario">
                    <Button
                      size="large"
                      className="green white--text"
                      icon="user-add"
                      loading={loading}
                      onClick={() => this.addUser()}
                      disabled={loading}
                    />
                  </Tooltip>
                </Col>
                <Col xs={8}>
                  <Tooltip placement="top" title="Refrescar">
                    <Button
                      size="large"
                      icon="sync"
                      loading={loading}
                      onClick={() => this.fetchUsers()}
                      disabled={loading}
                    />
                  </Tooltip>
                </Col>
                <Col xs={8}>
                  <Tooltip placement="top" title="Limpiar filtros">
                    <Button
                      size="large"
                      icon="sliders"
                      loading={loading}
                      onClick={() => this.clearFilters()}
                      disabled={loading}
                    />
                  </Tooltip>
                </Col>
              </Row>
            }
          >
            <Table
              size="middle"
              rowKey={record => record.id}
              loading={loading}
              columns={columns}
              dataSource={users.data}
              scroll={{ x: 1500 }}
              pagination={{
                className: 'pr-2',
                current: parseInt(users.page),
                defaultPageSize: parseInt(users.perPage),
                hideOnSinglePage: true,
                pageSize: parseInt(users.perPage),
                total: parseInt(users.total),
              }}
              onChange={(pagination, filters, sorter) => {
                this.setState({
                  sortField: sorter.field,
                  sortOrder: sorter.order === 'descend' ? 'desc' : 'asc',
                }, () => {
                  this.fetchUsers(pagination.current, pagination.pageSize);
                });
              }}
            />
          </Card>
        </Row>
        <UserForm
          ref={this.userForm}
          row={row}
          show={show}
          onOk={this.saveUser}
          onCancel={this.cancelEdit}
          roles={roles}
          areas={areas}
          loading={loading}
        />
      </Layout>
    );
  }
}

export default UserList;
