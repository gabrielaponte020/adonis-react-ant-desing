/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import isEmpty from 'lodash/isEmpty';
import axios from 'axios';
import moment from 'moment';
import StringMask from 'string-mask';
import { getLocalUser } from '../actions/user';

moment.locale('es');

/**
 * Method to get js default modules
 * @param files
 * @param isArray
 * @returns {*}
 */
export function getJsModules(files, isArray = false) {
  const modules = isArray ? [] : {};
  files.keys().forEach((key) => {
    if (key === './index.js') return;
    if (isArray) modules.push(files(key).default);
    else modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default;
  });
  return modules;
}

/**
 * Method for protect routes
 * @param Component
 * @param rest
 * @returns {*}
 */
export function requireAuth({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        (!isEmpty(getLocalUser()) ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/auth/login',
              state: { from: props.location },
            }}
          />
        ))
      }
    />
  );
}

export function notRequireAuth({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        (isEmpty(getLocalUser()) ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/',
              state: { from: props.location },
            }}
          />
        ))
      }
    />
  );
}

/**
 * Method for validate inputs
 * @param rules
 * @param value
 * @returns {*|null}
 */
export function validateRules(rules, value) {
  const errorBucket = [];
  rules.map((rule) => {
    const valid = typeof rule === 'function' ? rule(value) : rule;

    if (valid === false || typeof valid === 'string') {
      errorBucket.push(valid);
    } else if (valid !== true) {
      consoleError(`Las reglas solo retornan un string o un boolean, esta recibiendo '${typeof valid}' en su lugar`);
    }
  });

  return errorBucket[0] || null;
}


/**
 * Method for validate all rules
 * @param rules
 * @param value
 * @returns {*|null}
 */
export function validateAllRules(rules, obj) {
  const keys = Object.keys(rules);
  const errorBucket = [];
  keys.map((key) => {
    rules[key].map((rule) => {
      const valid = typeof rule === 'function' ? rule(obj[key]) : rule;

      if (valid === false || typeof valid === 'string') {
        errorBucket.push(valid);
      } else if (valid !== true) {
        consoleError(`Las reglas solo retornan un string o un boolean, esta recibiendo '${typeof valid}' en su lugar`);
      }
    });
  });

  return errorBucket[0] || null;
}

export function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export function setAuthorizationToken(token) {
  if (!isEmpty(token)) axios.defaults.headers.common.Authorization = `Bearer ${token.token}`;
  else delete axios.defaults.headers.common.Authorization;
}

export const dateFormat = (val, format) => moment(val).format((format || 'DD/MM/YYYY'));

export const defaultDelimiters = /[-!$%^&*()_+|~=`{}[\]:";'<>?,./\\ ]/;

export const unmaskText = text => (text ? String(text).replace(new RegExp(defaultDelimiters, 'g'), '') : text);

export const maskFormat = (val, format) => {
  const formatter = new StringMask(format);
  return formatter.apply(unmaskText(val));
};

export const setStateAsync = ($this, state) => new Promise((resolve) => {
  $this.setState(state, resolve);
});


export const isBase64 = (str) => {
  try {
    return btoa(atob(str)) === str;
  } catch (err) {
    return false;
  }
};
