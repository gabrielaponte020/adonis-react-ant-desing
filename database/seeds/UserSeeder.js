

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const User = use('App/Models/User');
const Factory = use('Factory');

class UserSeeder {
  async run() {
    const users = [
      {
        name: 'admin',
        lastname: 'admin',
        email: 'admin@admin.com',
        password: '123456789',
        rut: '00.000.000-0',
        role_id: 1,
      },
    ];

    await User.createMany(users);

    await Factory
      .model('App/Models/User')
      .createMany(100);
  }
}

module.exports = UserSeeder;
