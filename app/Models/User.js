
const Model = use('Model');

class User extends Model {
  static boot() {
    super.boot();

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeCreate', 'UserHook.hashPassword');
    this.addHook('beforeUpdate', 'UserHook.commonFields');
    this.addHook('afterDelete', 'UserHook.deleteFiles');
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token');
  }

  static get hidden() {
    return ['password'];
  }

  role() {
    return this.hasOne('App/Models/Role', 'role_id', 'id');
  }

  area() {
    return this.hasOne('App/Models/Area', 'area_id', 'id');
  }
}

module.exports = User;
