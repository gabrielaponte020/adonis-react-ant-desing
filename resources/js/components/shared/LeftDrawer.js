import React, { PureComponent } from 'react';
import { Layout, Menu, Icon } from 'antd';
import PropTypes from 'prop-types';
import logo from '../../../images/logo.png';

class LeftDrawer extends PureComponent {
  render() {
    const {
      // toogleDrawer,
      // isDrawer,
      history,
    } = this.props;
    return (
      <Layout.Sider
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
          backgroundColor: '#232C3a',
        }}
        breakpoint="lg"
        collapsedWidth="0"
        // onBreakpoint={(broken) => { console.log(broken); }}
      >
        <div
          className="logo"
        >
          <img alt="foodly-logo" src={logo} />
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[history.location.pathname.toString()]}
          style={{
            backgroundColor: '#232C3a',
          }}
        >
          <Menu.Item
            key="/"
            onClick={() => {
              history.push('/');
            }}
          >
            <Icon type="dashboard" />
            <span className="nav-text">Dashboard</span>
          </Menu.Item>
        </Menu>
      </Layout.Sider>
    );
  }
}


LeftDrawer.propTypes = {
  history: PropTypes.object.isRequired,
  // isDrawer: PropTypes.bool.isRequired,
  // toogleDrawer: PropTypes.func.isRequired,
  // classes: PropTypes.object.isRequired,
};

export default LeftDrawer;
