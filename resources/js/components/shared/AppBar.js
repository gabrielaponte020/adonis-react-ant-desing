import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { Layout, Menu, Dropdown, Avatar } from 'antd';
import { logout } from '../../actions/user';

class AppBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      // menuUser: null,
    };
    this.logout = this.logout.bind(this);
    this.getAvatar = this.getAvatar.bind(this);
  }

  getAvatar() {
    const { user } = this.props;
    if (user && user.photo) return { src: `/uploads/user/${user.photo}` };
    return { icon: 'user' };
  }

  async logout() {
    await this.props.logout(null);
  }

  render() {
    // const { menuUser } = this.state;

    const menu = (
      <Menu>
        <Menu.Item onClick={this.logout}>
          Cerrar sesióon
        </Menu.Item>
      </Menu>
    );

    return (
      <Layout.Header
        className="d-flex justify-space-between align-center"
        style={{
          position: 'fixed',
          zIndex: 1,
          width: '100%',
          backgroundColor: '#192537',
          paddingRight: 200,
        }}
      >
        <div />
        <div className="pr-2">
          <Dropdown overlay={menu} className="right">
            <Avatar className="app-bar-avatar" size="large" {...this.getAvatar()} />
          </Dropdown>
        </div>
      </Layout.Header>
    );
  }
}


AppBar.propTypes = {
  // toogleDrawer: PropTypes.func.isRequired,
  // isDrawer: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  isDrawer: state.shared.isDrawer,
  isAuth: state.user.isAuth,
  user: state.user.user.data,
});


export default compose(
  connect(mapStateToProps, { logout }),
  withRouter,
)(AppBar);
