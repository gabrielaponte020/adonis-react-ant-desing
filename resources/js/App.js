import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';
import { connect } from 'react-redux';
import { getLocalUser, logout, setCurrentUser } from './actions/user';

class App extends Component {
  componentDidMount() {
    const user = getLocalUser();
    this.props.setCurrentUser(user);
    axios.interceptors.response.use(res => res, (err) => {
      if (err.response.status === 401) {
        this.props.logout();
      }
      return Promise.reject(err);
    });
  }

  render() {
    return (
      <Router>
        {this.props.routes}
      </Router>
    );
  }
}

App.propTypes = {
  routes: PropTypes.element.isRequired,
  logout: PropTypes.func.isRequired,
  setCurrentUser: PropTypes.func.isRequired,
};

export default connect(null, { logout, setCurrentUser })(App);
