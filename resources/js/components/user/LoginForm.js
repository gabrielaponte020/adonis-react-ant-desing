import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Form, Icon, Input, Button, Checkbox, Row, Col, message } from 'antd';
import { hasErrors } from '../../util/index';
import api from '../../api/user';
import { setCurrentUser } from '../../actions/user';
import LoginImage from '../../../images/login.png';


const FormItem = Form.Item;

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    this.props.form.validateFields();
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loading: true }, async () => {
          try {
            const { data } = await api.login(values);
            this.props.setCurrentUser(data);
            this.props.history.push('/');
          } catch (error) {
            this.setState({ loading: false }, () => {
              try {
                if (error.response.status === 401) { message.error('Usuario o contraseña invalida'); }
              } catch (c) {
                console.log(c);
                message.error('Ocurrio un error intente de nuevo');
              }
            });
          }
        });
      }
    });
  }

  render() {
    const {
      getFieldDecorator, getFieldsError, isFieldTouched, getFieldError,
    } = this.props.form;

    const { loading } = this.state;

    // Only show error after a field is touched.
    const userNameError = isFieldTouched('email') && getFieldError('email');
    const passwordError = isFieldTouched('password') && getFieldError('password');

    return (
      <Row className="login-form-wrapper elevation-ant d-flex">
        <Col xs={{ span: 24 }} lg={{ span: 12 }} className="d-flex align-center justify-center">
          <Form onSubmit={this.handleSubmit} className="login-form">
            <FormItem
              validateStatus={userNameError ? 'error' : ''}
              help={userNameError || ''}
            >
              {getFieldDecorator('email', {
                rules: [
                  {
                    required: true,
                    pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                    message: 'Email invalido',
                  },
                ],
              })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />)}
            </FormItem>
            <FormItem
              validateStatus={passwordError ? 'error' : ''}
              help={passwordError || ''}
            >
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Password requerido' }],
              })(<Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox>Recuerdame</Checkbox>)}
              <a className="login-form-forgot" href="/#">Olvide mi contraseña</a>
              <Button
                loading={loading}
                type="primary"
                htmlType="submit"
                className="login-form-button"
                disabled={hasErrors(getFieldsError())}
              >
                Iniciar Sesión
              </Button>
            </FormItem>
          </Form>
        </Col>
        <Col xs={{ span: 24 }} lg={{ span: 12 }}>
          <img style={{ width: '100%' }} alt="foodly" src={LoginImage} />
        </Col>
      </Row>
    );
  }
}

LoginForm.propTypes = {
  history: PropTypes.object.isRequired,
  setCurrentUser: PropTypes.func.isRequired,
  form: PropTypes.object.isRequired,
};

export default compose(
  Form.create(),
  connect(null, { setCurrentUser }),
)(LoginForm);

