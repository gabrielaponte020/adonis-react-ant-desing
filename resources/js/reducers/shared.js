import { TOOGLE_DRAWER } from '../actions/types';

const initialState = {
  isDrawer: false,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case TOOGLE_DRAWER:
      return {
        isDrawer: action.isDrawer,
      };
    default: return state;
  }
};
