import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { getJsModules, requireAuth as AuthRute } from '../util/index';
import Layout from '../layouts/Layout';
import Admin from '../pages/user/Admin';

// Get all modules
const modules = getJsModules(require.context('.', false, /\.js$/), true);

/**
 * Routes for parent module
 */
export default (
  <Route
    render={({ location }) => (
      <Layout>
        <Switch location={location}>
          <AuthRute
            key="home"
            exact
            path="/"
            component={Admin}
          />
          {modules.map(m => m)}
        </Switch>
      </Layout>
      )}
  />
);

