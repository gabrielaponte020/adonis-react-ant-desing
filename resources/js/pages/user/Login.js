import React, { Component } from 'react';
import Loadable from 'react-loadable';
import Loading from '../../components/common/Loading';

const LoginForm = Loadable({
  loader: () => import('../../components/user/LoginForm'),
  loading: () => <Loading size="large" />,
});

class Login extends Component {
  render() {
    return (
      <div className="login-main fill-height d-flex align-center justify-center">
        <LoginForm {...this.props} />
      </div>
    );
  }
}

export default Login;

