const Area = use('App/Models/Area');

class AreaController {
  /**
   * List
   * @returns {Promise<*>}
   */
  async index() {
    const areas = await Area.query();

    return areas;
  }
  /**
   * Find
   * @param request
   * @returns {Promise<*>}z
   */
  async show({ request }) {
    const { id } = request.all();
    const area = await Area.find(id);
    return area;
  }

  /**
   * Create
   * @param request
   * @returns {Promise<*>}
   */
  async store({ request }) {
    const area = request.only(['type', 'company_id', 'name', 'lastname', 'photo', 'email', 'password']);
    const result = await Area.create(area);
    return result;
  }

  /**
   * Update
   * @param request
   * @returns {Promise<void>}
   */
  async update({ request, auth }) {
    const {
      id,
      name,
      lastname,
      email,
      type,
      password,
      company_id,
    } = request.all();
    const area = await Area.find(id);
    area.merge({
      id,
      name,
      lastname,
      email,
      type,
      password,
      company_id,
    });
    await area.save();

    const authrole = await auth.generate(area, true);

    return authrole;
  }

  /**
   * Delete
   * @param request
   * @returns {Promise<void>}
   */
  async destroy({ params }) {
    const area = await Area.find(params.id);
    await area.delete();
  }
}

module.exports = AreaController;
